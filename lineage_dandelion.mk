#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)

# Inherit from LineageOS vendor
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Some build flags
TARGET_BOOT_ANIMATION_RES := 720
TARGET_FACE_UNLOCK := true
TARGET_USES_BLUR := true
TARGET_SUPPORTS_GOOGLE_RECORDER := true
TARGET_INCLUDE_LIVE_WALLPAPERS := false

# Inherit from dandelion device makefile
$(call inherit-product, $(LOCAL_PATH)/device.mk)

# Device identifier. This must come after all inclusions.
PRODUCT_NAME := lineage_dandelion
PRODUCT_DEVICE := dandelion
PRODUCT_BRAND := Redmi
PRODUCT_MODEL := M2006C3LC
PRODUCT_MANUFACTURER := Xiaomi

# Build info
TARGET_VENDOR := xiaomi
TARGET_VENDOR_PRODUCT_NAME := dandelion

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi

# PRODUCT_VENDOR_PROPERTIES
PRODUCT_VENDOR_PROPERTIES += \
    ro.vendor.ok=yes \
    ro.vendor.test.ok=yes

# PRODUCT_DEFAULT_PROPERTY_OVERRIDES
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    ro.sf.lcd_density=320 \
    ro.vendor.rc=/vendor/etc/init/hw/ \
    ro.oem_unlock_supported=1 \
    ro.vendor.init.sensor.rc=init.sensor_1_0.rc \
    camera.disable_zsl_mode=1 \
    ro.logd.kernel=false \
    persist.sys.timezone=Asia/Shanghai \
    debug.sf.disable_backpressure=1

