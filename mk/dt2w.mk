# DT2W
PRODUCT_PACKAGES += \
    DT2W-Service-Dandelion

PRODUCT_COPY_FILES += \
    $(DEVICE_PATH)/dt2w/dt2w_event:$(TARGET_COPY_OUT_SYSTEM)/bin/dt2w_event

PRODUCT_COPY_FILES += \
    $(DEVICE_PATH)/dt2w/dt2w_event.rc:$(TARGET_COPY_OUT_SYSTEM)/etc/init/dt2w_event.rc

